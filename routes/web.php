<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');


Route::get('/table', function(){
    return view('items.table');
})->name('table');
Route::get('/data-tables', function(){
    return view('items.data_table');
})->name('data_table');

Route::get('/cast', 'CastController@index')->name('cast.index');
Route::get('/cast/create', 'CastController@create')->name('cast.create');
Route::post('/cast', 'CastController@store')->name('cast.store');
Route::get('/cast/{cast_id}', 'CastController@show')->name('cast.show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit')->name('cast.edit');
Route::put('/cast/{cast_id}', 'CastController@update')->name('cast.update');
Route::delete('/cast/{cast_id}', 'CastController@destroy')->name('cast.destroy');