@extends('adminlte.master')
@section('title')
    Create Account
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h1>Buat Account Baru!</h1>
                    <h2>Sign Up Form</h2>
                    <form action="{{route('welcome')}}" method="POST">
                        @csrf
                        <label for="first_name">First name:</label>
                        <br><br>
                        <input type="text" name="first_name" id="first_name">
                        <br><br>
                        <label for="last_name">Last name:</label>
                        <br><br>
                        <input type="text" name="last_name" id="last_name">
                        <br><br>
                        <label>Gender</label>
                        <br><br>
                        <input type="radio" name="gender" id="male" value="Male">
                        <label for="male">Male</label><br>
                        <input type="radio" name="gender" id="female" value="Female">
                        <label for="female">Female</label><br>
                        <input type="radio" name="gender" id="other" value="Other">
                        <label for="other">Other</label>
                        <br><br>
                        <label for="nationality">Nationality:</label>
                        <br><br>
                        <select name="nationality" id="nationality">
                            <option value="indonesian">Indonesian</option>
                            <option value="singaporean">Singaporeans</option>
                            <option value="malaysian">Malaysians</option>
                            <option value="australian">Australian</option>
                        </select>
                        <br><br>
                        <label>Language Spoken:</label>
                        <br><br>
                        <input type="checkbox" name="language" id="indonesia">
                        <label for="indonesia">Bahasa Indonesia</label> <br>
                        <input type="checkbox" name="language" id="english">
                        <label for="english">English</label> <br>
                        <input type="checkbox" name="language" id="other">
                        <label for="other">Other</label> <br>
                        <br>
                        <label for="bio">Bio:</label>
                        <br><br>
                        <textarea name="biodata" id="bio" cols="30" rows="10"></textarea>
                        <br>
                        <input type="submit" value="Sign Up">
                    </form>
                </div>
            </div>
            
        </div>
    </div>
@endsection
        