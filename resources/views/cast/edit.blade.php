@extends('adminlte.master')
@section('title')
    Cast Edit
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Create Edit</h3>
                </div>
                <form role="form" action="{{route('cast.update', $cast->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label>Cast Name</label>
                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Enter Cast Name" value="{{old('nama')?? $cast->nama}}">
                            @error('nama')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Cast Age</label>
                            <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="Enter Cast Age" value="{{old('umur') ?? $cast->umur}}">
                            @error('umur')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Cast BIO</label>
                            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" cols="30" rows="10">{{old('bio') ?? $cast->bio}}</textarea>
                            @error('bio')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
              </div>
        </div>
    </div>
@endsection