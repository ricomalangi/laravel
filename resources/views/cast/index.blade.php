@extends('adminlte.master')
@section('title')
    Data casts
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('cast.create')}}" class="btn btn-md btn-success">Create Cast</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <table class="table">
                    <thead>
                      <tr>
                        <th style="width: 4%">id</th>
                        <th style="width: 50%">Name</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @php
                        $id = 1;
                    @endphp
                    @foreach ($casts as $cast)
                    <tr>
                        <td>{{$id++}}</td>
                        <td>{{$cast->nama}}</td>
                        <td>
                            <a href="{{route('cast.show', $cast->id)}}" class="btn btn-md btn-primary">Show</a>
                            <a href="{{route('cast.edit', $cast->id)}}" class="btn btn-md btn-warning">Edit</a>
                            <form action="{{route('cast.destroy', $cast->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-md btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr> 
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
        </div>
    </div>
@endsection