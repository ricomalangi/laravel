@extends('adminlte.master')
@section('title')
    Cast Detail
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Create Detail</h3>
                </div>
                <form>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Cast Name</label>
                            <input type="text" name="nama" class="form-control" placeholder="Enter Cast Name" value="{{$cast->nama}}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Cast Age</label>
                            <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="Enter Cast Age" value="{{$cast->umur}}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Cast BIO</label>
                            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" cols="30" rows="10" readonly>{{$cast->bio}}</textarea>
                        </div>
                    </div>
                </form>
              </div>
        </div>
    </div>
@endsection