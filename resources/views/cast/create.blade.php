@extends('adminlte.master')
@section('title')
    Cast Create
@endsection
@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Create Cast</h3>
                </div>
                <form role="form" action="{{route('cast.store')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Cast Name</label>
                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Enter Cast Name" value="{{old('nama')}}">
                            @error('nama')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Cast Age</label>
                            <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="Enter Cast Age" value="{{old('umur')}}">
                            @error('umur')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Cast BIO</label>
                            <textarea class="form-control @error('bio') is-invalid @enderror" name="bio" cols="30" rows="10">{{old('bio')}}</textarea>
                            @error('bio')
                            <div class="error invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div>
        </div>
    </div>
@endsection