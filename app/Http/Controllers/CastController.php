<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cast;
class CastController extends Controller
{
 
    public function index()
    {
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }


    public function create()
    {
        return view('cast.create');
    }

    public function show($id){
        $cast = Cast::where('id', $id)->first();
        return view('cast.show', [
            'cast' => $cast 
        ]);
    }
  
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required'
        ]);
        Cast::insert([
            'nama' => request('nama'),
            'umur' => request('umur'),
            'bio' => request('bio')
        ]);
        return redirect()->route('cast.create');
    }

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast.edit', [
            'cast' => $cast 
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required'
        ]);
        Cast::where('id', $id)->update([
            'nama' => request('nama'),
            'umur' => request('umur'),
            'bio' => request('bio')
        ]);
        return redirect()->route('cast.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cast::where('id', $id)->delete();
        return redirect()->route('cast.index');
    }
}
